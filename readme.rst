======================================================
 structImp: adds a magic touch to the data structures
======================================================

What is it
==========
structImp is a set of tools designed to simplify the work with tree-like data structures. It includes functions for
**advance loading and saving data structures** from a range of supported file formats (by third parties packages). By
using designated *meta data* fields, marked by leading "\_\_\_" (triple underscore) to the field's name, the package
adds extended capability to the load\\save process such as cross-referencing data between files, templates and
converting relative in to absolute paths. The package also include basic data structure function such as merge and
compare.

Main features
=============
- Spanning data structures among multiple files\folders.
- Converting relative paths to absolute paths
- Template strings\paths
- Data type specifying
- Creating numeric lists (linear & logarithmic spaced)
- Data structures merging & comparing

Supported file formats
======================
- Ascii (as a string) (associated with: '*txt*')
- JSON: using python's built in json package (associated with: '*JSON*')
- yaml: using pyyaml (associated with: '*yml*', '*yaml*')
- Images: using scipy and PIL (associated with: '*png*', '*jpg*', '*jpeg*')

