import copy
import os
import sys

from defines import FileType
from defines import EXTENSIONS_MAPPINGS
from .errors import CannotFindFileError
from .errors import UnknownExtensionError
from .errors import MissingDependency


def _load(filename, file_type=None):

    filename = os.path.normpath(os.path.abspath(filename))
    if not sys.platform == "win32":
        filename = filename.replace('\\', '/')

    if not os.path.isfile(filename):
        raise CannotFindFileError('No file named: %s' % filename)

    # getting file type
    if file_type is None:
        file_extension = os.path.splitext(filename)[1][1:].lower()
        file_type = EXTENSIONS_MAPPINGS.get(file_extension, FileType.unknown)

    # loading file
    if file_type == FileType.ascii:
        with open(filename, 'r') as file_stream:
            data_struct = file_stream.read()

    elif file_type == FileType.yaml:
        try:
            import yaml
            with open(filename, 'r') as file_stream:
                data_struct = yaml.load(file_stream)
        except ImportError:
            raise MissingDependency('Could not import %s\n. To load YAML files install "pyyaml" package' % filename)

    elif file_type == FileType.json:
        try:
            import json
            with open(filename, 'r') as file_stream:
                data_struct = json.load(file_stream)
        except ImportError:
            raise MissingDependency('Could not import %s\n. To load JSON files install "json" package' % filename)

    elif file_type == FileType.image:
        try:
            from scipy import misc
            data_struct = misc.imread(filename)
        except ImportError:
            raise MissingDependency('Could not import %s\n. To load image files install "scipy", "numpy" and "PIL" package' % filename)

    else:
        raise UnknownExtensionError('Could not import %s. Unknown file extension %s' % (filename, file_extension))

    return data_struct
