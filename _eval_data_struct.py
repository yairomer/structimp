import os
import sys
import copy
import re

from ._evaloators import _eval_template
from ._load import _load
from .utills import _merge_data_structs

from .errors import NonStructuredAttribute


def _load_and_eval(filename, meta_fields_struct=None, file_type=None, local_vars=None):

    if local_vars is None:
        local_vars = {}

    data_struct = _load(filename, file_type=file_type)

    local_vars = copy.copy(local_vars)
    filename = os.path.normpath(os.path.abspath(filename))
    if not sys.platform == "win32":
        filename = filename.replace('\\', '/')

    local_vars['base_folder'] = os.path.dirname(filename)

    data_struct = _eval_data_struct(data_struct, meta_fields_struct=meta_fields_struct, local_vars=local_vars)

    return data_struct


def _eval_data_struct(data_struct, meta_fields_struct=None, local_vars=None):

    if meta_fields_struct is None:
        meta_fields_struct = {}

    if local_vars is None:
        local_vars = {}

    if type(data_struct) is dict:  # evaluate only if dictionary
        # collect evaluators from designated meta field
        if 'meta___' in data_struct:
            if 'meta___' not in meta_fields_struct:
                meta_fields_struct['meta___'] = {}
            meta_fields_struct['meta___'] = data_struct['meta___']
            data_struct.pop('meta___')

        # collect evaluators from nested meta field
        curr_eval_mask = re.compile(r'^meta___(.*)$')  # current evaluator mask
        for field in data_struct.keys():
            match = curr_eval_mask.match(field)
            if match:
                evaluator_name = match.group(1)
                evaluator_value = data_struct[field]
                if 'meta___' not in meta_fields_struct:
                    meta_fields_struct['meta___'] = {}
                meta_fields_struct['meta___'][evaluator_name] = evaluator_value
                data_struct.pop(field)

    if 'meta___' in meta_fields_struct:
        # evaluate meta fields
        # variables
        if 'var' in meta_fields_struct['meta___']:
            vars_list = _eval_data_struct(meta_fields_struct['meta___']['var'], local_vars=local_vars)
            if type(vars_list) is not dict:
                raise NonStructuredAttribute()
            local_vars = copy.deepcopy(local_vars)
            for variable_name in vars_list.keys():
                local_vars[variable_name] = vars_list[variable_name]
        # data
        if 'data' in meta_fields_struct['meta___']:
            data_struct = meta_fields_struct['meta___']['data']
        # template
        if 'eval' in meta_fields_struct['meta___']:
            data_struct = _eval_template(meta_fields_struct['meta___']['eval'], local_vars=local_vars)
        # load
        elif 'load' in meta_fields_struct['meta___']:
            filename = _eval_template(meta_fields_struct['meta___']['load'], local_vars=local_vars)
            meta_fields_struct_tmp = copy.deepcopy(meta_fields_struct)
            meta_fields_struct_tmp.pop('meta___')
            data_struct = _load_and_eval(filename, meta_fields_struct=meta_fields_struct_tmp, local_vars=local_vars)
        elif 'load' in meta_fields_struct['meta___']:
            filename = _eval_template(meta_fields_struct['meta___']['load'], local_vars=local_vars)
            meta_fields_struct_tmp = copy.deepcopy(meta_fields_struct)
            meta_fields_struct_tmp.pop('meta___')
            data_struct = _load_and_eval(filename, meta_fields_struct=meta_fields_struct_tmp, local_vars=local_vars)

    if type(data_struct) is dict:  # evaluate only if dictionary
        # collect children evaluators
        curr_eval_mask = re.compile(r'^(.+)___(.*)$')  # current evaluator mask
        for field in data_struct.keys():
            match = curr_eval_mask.match(field)
            if match:
                field_name = match.group(1)
                evaluator_name = match.group(2)
                if not evaluator_name:  # set default for empty evaluator
                    evaluator_name = 'eval'
                evaluator_value = data_struct[field]
                if field_name not in meta_fields_struct:
                    meta_fields_struct[field_name] = {}
                if 'meta___' not in meta_fields_struct[field_name]:
                    meta_fields_struct[field_name]['meta___'] = {}
                meta_fields_struct[field_name]['meta___'][evaluator_name] = evaluator_value
                data_struct.pop(field)

        # evaluate children evaluators
        fields_list = set(data_struct.keys()) \
            | set([field for field in meta_fields_struct.keys() if field is not 'meta___'])
        for field_name in fields_list:
            evaluators_struct_tmp = meta_fields_struct.get(field_name, {})
            data_struct_tmp = data_struct.get(field_name, None)
            data_struct[field_name] = _eval_data_struct(data_struct_tmp,
                                                        meta_fields_struct=evaluators_struct_tmp,
                                                        local_vars=local_vars)
            if evaluators_struct_tmp:
                meta_fields_struct[field_name] = evaluators_struct_tmp

    if 'meta___' in meta_fields_struct:
        # evaluate meta fields
        # defaults
        if 'defaults' in meta_fields_struct['meta___']:
            data_struct_tmp = _eval_data_struct(meta_fields_struct['meta___']['defaults'],
                                                local_vars=local_vars)
            data_struct = _merge_data_structs(data_struct_tmp, data_struct)

    return data_struct

