from ._eval_data_struct import _load_and_eval
from ._save import _save
from ._eval_data_struct import _eval_data_struct
from .utills import _merge_data_structs
from .utills import _print_data_struct


def load(filename, file_type=None, local_vars=None, meta_fields_struct=None):
    """
    Loads a file and evaluates the meta fields.

    Parameters
    ----------
    filename: str
        The name of the file to load.
    file_type: str
        If not specified the file type is the file extension.
    local_vars: dict
        A dictionary of variables which are use evaluate templates.
        Organized as: {variable1: value1, variable2: value2, ...}
    meta_fields_struct: dict
        A meta field struct could be given at the call to the function (see documentation for more info).
        meta_fields_struct is changed inline so an variable containing an empty dictionary can be passes to retrieve the
        output meta_fields_struct.

    Returns:
        data_struct

    Raises:
        CannotFindFileError: if file dose not exist
        UnknownExtensionError: for an unknown extension
        NonStructuredAttribute: for an attribute that is not a dictionary
        MissingDependency: when a specific python package is missing to load a certain file type
    """

    if meta_fields_struct is None:
        meta_fields_struct = {}

    if local_vars is None:
        local_vars = {}

    data_struct = _load_and_eval(filename, meta_fields_struct=meta_fields_struct, file_type=file_type,
                                 local_vars=local_vars)

    return data_struct


def save(filename, data_struct, file_type=None):
    """
    Saves a file

    Parameters
    ----------
    filename: str
        The name of the file to save to.
    data_struct
    file_type: str
        If not specified the file type is the file extension.

    Raises:
        UnknownExtensionError: for an unknown extension
        MissingDependency: when a specific python package is missing to load a certain file type
    """

    _save(filename, data_struct, file_type=file_type)


def eval_data_struct(data_struct, local_vars=None, meta_fields_struct=None):
    """
    Evaluate attributes in a data structure.

    Parameters
    ----------
    data_struct
    local_vars: dict
        A dictionary of variables which are use evaluate templates.
         Organized as: {variable1: value1, variable2: value2, ...}
    meta_fields_struct: dict
        An evaluator struct could be given at the call to the function (see documentation for more info).
        meta_fields_struct is changed in line so an variable containing an empty dictionary can be passes to retrieve the
        output meta_fields_struct.

    Returns:
        data_struct

    Raises:
        CannotFindFileError: if file dose not exist
        UnknownExtensionError: for an unknown extension
        NonStructuredAttribute: for an attribute that is not a dictionary
        MissingDependency: when a specific python package is missing to load a certain file type
    """

    if local_vars is None:
        local_vars = {}

    data_struct = _eval_data_struct(data_struct, meta_fields_struct=meta_fields_struct, local_vars=local_vars)

    return data_struct


def merge_data_structs(data_struct1, data_struct2):
    """
    Merge two data structures with priority to data_struct2.

    Parameters
    ----------
    data_struct1, data_struct2

    Returns:
        data_struct
    """

    return _merge_data_structs(data_struct1, data_struct2)


def print_data_struct(data_struct, print_type='yaml'):
    """
    Pretty prints a data structure.

    Parameters
    ----------
    data_struct
    print_type: string
        The format in wich to print the data structure.
        Options: 'python', 'yaml'
        Default: 'yaml'

    Raises:
        MissingDependency: when a specific python package is missing to print in a certain format
    """

    _print_data_struct(data_struct, print_type)
