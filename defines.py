class FileType(object):
    unknown = 'unknown'
    ascii = 'ascii'
    json = 'json'
    yaml = 'yaml'
    image = 'image'



EXTENSIONS_MAPPINGS = {
    'txt': FileType.ascii,
    'json': FileType.json,
    'yml': FileType.yaml,
    'yaml': FileType.yaml,
    'png': FileType.image,
    'jpg': FileType.image,
    'jpeg': FileType.image,
}
