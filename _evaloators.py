import os
import re

from .errors import MissingKeyInDatabase


def _get_var(locator, local_vars):
    split_locator = re.split(r'\.', locator)
    value = local_vars
    for val in split_locator:
        if type(value) is dict and val in value:
            value = value[val]
        else:
            raise MissingKeyInDatabase('Could load "%s" from variables.\n Missing the key "%s"' % (locator, val))

    return value


def _eval_template(template_string, local_vars=None):

    if local_vars is None:
        local_vars = {}

    if type(template_string) is list:
        for i in range(len(template_string)):
            template_string[i] = _eval_template(template_string[i], local_vars=local_vars)
        return template_string
    elif type(template_string) is dict:
        for field_name in template_string.keys():
            template_string[field_name] = _eval_template(template_string[field_name], local_vars=local_vars)
        return template_string
    elif not type(template_string) is str:
        return template_string

    variable_mask = r'\$(.+)\$'  # variables mask
    rpl_func = lambda matchobj:  _get_var(matchobj.group(1), local_vars=local_vars)

    template_string = re.sub(variable_mask, rpl_func, template_string)

    if (len(template_string) >= 2) and (template_string[0:2] in ['.\\', './']) \
            or (len(template_string) >= 3) and (template_string[0:3] in ['..\\', '../']):

        template_string = os.path.normpath(os.path.join(local_vars['base_folder'], template_string))

    elif (len(template_string) >= 4) and (template_string[0:4] in ['...\\', '.../']) \
            or (len(template_string) >= 5) and (template_string[0:5] in ['....\\', '..../']):
        template_string = template_string[2:]

    return template_string
