import os

import structimp


def test_load():
    data_struct_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data', 'data_struct_1.yml')

    evaluators_struct = {}
    data_struct = structimp.load(data_struct_file, meta_fields_struct=evaluators_struct)

    structimp.print_data_struct(data_struct)
    structimp.print_data_struct(evaluators_struct)

if __name__ == '__main__':
    test_load()
