import copy


from .errors import MissingDependency


def _merge_data_structs(data_struct1, data_struct2):
    if data_struct1 is None:
        return data_struct2
    elif data_struct2 is None:
        return data_struct1
    elif (type(data_struct1) is dict) and (type(data_struct2) is dict):

        keys1 = set(data_struct1.keys())
        keys2 = set(data_struct2.keys())

        data_struct = copy.copy(data_struct1)
        for key in (keys2 - keys1):
            data_struct[key] = data_struct2[key]

        for key in (keys2 & keys1):
            data_struct[key] = _merge_data_structs(data_struct1[key], data_struct2[key])

        return data_struct

    else:
        return data_struct2



def _print_data_struct(data_struct, print_type='yaml'):
    print_type = print_type.lower()

    if print_type in ('python', 'py'):
        print(data_struct)
    elif print_type in ('yaml', 'yml'):
        try:
            import yaml
        except ImportError:
            raise MissingDependency('Could not print data structure. To print YAML files install "pyyaml" package')
        print(yaml.safe_dump(data_struct, default_flow_style=False))
    else:
        print('!! fileImp error : unsupported print type : %s' % print_type)