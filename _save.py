import copy
import os
import sys

from defines import FileType
from defines import EXTENSIONS_MAPPINGS
from .errors import UnknownExtensionError
from .errors import MissingDependency

def _save(filename, data_struct, file_type=None):

    filename = os.path.normpath(os.path.abspath(filename))
    if not sys.platform == "win32":
        filename = filename.replace('\\', '/')

    # getting file type
    if file_type is None:
        file_extension = os.path.splitext(filename)[1][1:].lower()
        file_type = EXTENSIONS_MAPPINGS.get(file_extension, FileType.unknown)

    # Create directory if necessary
    if not os.path.isdir(os.path.dirname(filename)):
        os.mkdir(os.path.dirname(filename))

    # save file
    if file_type == FileType.ascii:
        with open(filename, 'w') as file_stream:
            file_stream.write(data_struct)

    elif file_type == FileType.yaml:
        try:
            import yaml
            with open(filename, 'w') as file_stream:
                yaml.dump(data_struct, file_stream)
        except ImportError:
            raise MissingDependency('Could not export %s\n. To save YAML files install "pyyaml" package' % filename)

    elif file_type == FileType.json:
        try:
            import json
            with open(filename, 'r') as file_stream:
                json.dump(data_struct, file_stream)
        except ImportError:
            raise MissingDependency('Could not export %s\n. To load JSON files install "json" package' % filename)

    elif file_type == FileType.image:
        try:
            from scipy import misc
            misc.imsave(filename, data_struct)
        except ImportError:
            raise MissingDependency('Could not export %s\n. To save image files install "scipy", "numpy" and "PIL" package' % filename)

    else:
        raise UnknownExtensionError('Could not export %s. Unknown file extension %s' % (filename, file_type))
