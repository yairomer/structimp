.. structimp documentation master file, created by
   sphinx-quickstart on Thu Nov 12 22:04:32 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

structimp Documentation
=======================

structImp is a set of tools designed to simplify the work with tree-like data structures. It includes functions for
**advance loading and saving data structures** from a range of supported file formats (by third parties packages). By
using designated *meta data* fields, marked by leading "\_\_\_" (triple underscore) to the field's name, the package
adds extended capability to the load\\save process such as cross-referencing data between files, templates and
converting relative in to absolute paths. The package also include basic data structure function such as merge and
compare.


Using structimp
===============

.. toctree::
   :maxdepth: 2

   meta_fields
   dependencies


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

