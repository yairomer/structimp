Variables and Meta Fields
=========================
To use structimp we must start by getting to know tree types of objects used by structimp: variables, templates and
meta fields.

Variables
---------
In structimp one define variables which could be use for evaluating templates and filling in fields. Variable can be
either a scalar values (a number or a string) or a data structure (dict). Variables can by either defined in the
arguments of structimp function's or by using specified meta fields (see the up coming section).

Templates
---------
structImp can use string templates to create formatted strings. Evaluating a template include two processes:

1.  Locating variables in the template which are defined by *{variable_name}* and replacing them with the variable
    named *variable_name*. To address a field in a data structure one can use *{variable_name.field1.field2}*.

..  To literally include a brace character in the string use double braces: {{ and }}.

2.  If the string starts with *.\\* , *..\\* , *./* or *../* then the string is assumed to be a relative path and is
    converted into a absolute path using the current file's folder as the base folder. To literally include one of the
    above sequences add additional double dot *..* which will be removed when formatting.
    (..../foo will be read as ../foo and .../foo will be read as ./foo).


Meta fields
-----------
The core entity of structImp are the *meta fields*. Meta fields can be thought of as the attributes of a certain node
(a field in the data structure) which can preform certain action during the loading of the data. The name of each
meta_field defines its action according to the list described in the following section. The structure of the
*meta fields* is as follow. Say we have a field named *foo* with some value *bar*:

    *foo*:

        *bar*

and we want to use two mata fields, of type *meta_type1* and *meta_field2*, with values *mata_value1* and
*meta_value2*. These meta fields can be defined in one of the following three ways:

As sibling fields
    The meta fields can be placed in the same level of the objective field, using the name
    *field_name___meta_field_type*. The structure is then:

        *foo___meta_type1*: *meta_value1*

        *foo___meta_type2*: *meta_value2*

        *foo* : *bar*

As nested fields
    The objective field can have one or more fields named *meta___meta_fields_type*  nested under in. The structure is
    then:

        *foo*:

            *meta___meta_type1*: *meta_value1*

            *meta___meta_type1*: *meta_value2*

            *bar*: *val*

Using a designated *meta___* field
    The objective field will have a special field named *meta___* containing a list of the meta fields and values.

        *foo*:

            *meta___*:

                *meta_type1*: *meta_value1*

                *meta_type2*: *meta_value2*

            *bar*: *val*

Note: The last two methods can only be used if *bar* is a data structure (dict) or if it dose not exist before
      evaluation of the meta fields.


A shortcut:
    The following form:

        *foo___*: *bar*

    This is equivalent to using:

        *foo___eval*: *bar*

    This could be used for quickly using an *eval* meta field


Meta Fields Types
~~~~~~~~~~~~~~~~~
In the above examples the type of the meta field, and therefore it's affect on *foo* and *bar* are defined by replacing
the *meta_type1* and *meta_type2* strings with the name on the meta field type. The available types and there affect
are:

*eval*:
    *meta_value*: *template_string*
    Evaluates *template_string* and sets *foo* to that value (*bar* if exist is deleted).

*var*:
    *meta_value*: {*name1*: *value1*, *name2*: *value2*, ...}
    Defines one or more variables (see variables section). If *value* is a string then it is evaluated as a template.

*load*:
    *meta_value*: *file_name*
    Loads data from the file *file_name* into *foo* (*bar* if exist is deleted).
    Before loading the file *file_name* is evaluated as a template.

*default*:
    Use *meta_value* as the default value for *foo*. If *bar* exists and *bar* and *meta_value* are both data structures
    then the two data structures are merged with priority for *bar*.

*data*:
    Simply sets *foo* to *meta_value* (*bar* if exist is deleted).
