"""
filesImp uses the following errors:
"""

class FileImpStackedException(Exception):
    def add_stack(self, new_stack_value):
        self.stack_list += new_stack_value


class CannotFindFileError(FileImpStackedException):
    """
    Is raised when file dose not exist
    """
    pass

class UnknownExtensionError(FileImpStackedException):
    """
    Is raised when file has an unknown extension
    """
    pass

class MissingDependency(FileImpStackedException):
    """
    Is raised when an external package needed to open or save a file is missing
    """
    pass

class NonStructuredAttribute(FileImpStackedException):
    """
    Is raised when an attribute is missing in a dictionary
    """
    pass

class MissingKeyInDatabase(FileImpStackedException):
    """
    Is raised when trying to access a non-existing key in a database
    """
    pass

